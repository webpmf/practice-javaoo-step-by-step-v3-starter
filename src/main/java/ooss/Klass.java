package ooss;

public class Klass {

    private int number;

    private Person attachedPerson;
    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object anotherKlass) {
        Klass class1 = (Klass) anotherKlass;
        return class1.number == this.number;
    }

    public void assignLeader(Student student) {
        if (student.getKlass() != null && this.number == student.getKlass().getNumber()) {
            student.setIsLeader(1);
            attachedPerson.speak(student);
        } else {
            System.out.println("It is not one of us.");
        }
    }

    public boolean isLeader(Student student) {
        if (student.getKlass() != null && student.getIsLeader() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public  void  attach(Person person){
        this.attachedPerson = person;
    }
}
