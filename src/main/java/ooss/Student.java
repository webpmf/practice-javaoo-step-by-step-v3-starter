package ooss;

public class Student extends Person{

    private Klass klass;

    private int isLeader;

    public void setKlass(Klass klass) {
        this.klass = klass;
    }

    public int getIsLeader() {
        return isLeader;
    }

    public void setIsLeader(int isLeader) {
        this.isLeader = isLeader;
    }

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String introduce1 = String.format("My name is %s. I am %d years old. I am a student. I am in class %d.",this.name,this.age,this.klass.getNumber());
        String introduce2 = String.format("My name is %s. I am %d years old. I am a student. I am the leader of class %d.",this.name,this.age,this.klass.getNumber());
        if(this.isLeader == 1){
            return introduce2;
        }
        else {
            return introduce1;
        }
    }

    public void join(Klass klass){
       this.klass = klass;
    }

    public Klass getKlass() {
        return klass;
    }

    public boolean isIn(Klass klass){
        if(this.klass == null){
            return false;
        }
        else {
            return this.klass==klass;
        }
    }

    @Override
    public void speak(Student student) {
        System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.",
                this.name, this.klass.getNumber(), student.getName()));
    }
}
