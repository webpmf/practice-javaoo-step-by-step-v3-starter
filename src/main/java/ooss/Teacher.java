package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {

    List<Klass> klasses = new ArrayList();
    private Student student;

    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    public String getTeacherClass(List<Klass> klasses) {
        return klasses.stream()
                .map(klass -> klass.getNumber())
                .map(String::valueOf)
                .collect(Collectors.joining(", "));
    }


    @Override
    public String introduce() {
        String introduce = super.introduce()
                + String.format(" I am a teacher. I teach Class %s.", getTeacherClass(klasses));
        return introduce;
    }


    public void assignTo(Klass klass) {
        klasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        if (klasses.contains(klass)) {
            return true;
        }
        return false;
    }

    public boolean isTeaching(Student student) {
        if (student.getKlass() == null) {
            return false;
        } else if (klasses.contains(student.getKlass())) {
            return true;
        }
        return false;
    }

    @Override
    public void speak(Student student) {
        System.out.println(String.format("I am %s, teacher of Class %s. I know %s become Leader.",
                this.name, getTeacherClass(klasses), student.getName()));
    }
}
