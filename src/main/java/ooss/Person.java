package ooss;

public class Person {
    private int id;
    String name;
    int age;


    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String introduce(){
        String introduce = String.format("My name is %s. I am %d years old.",this.name,this.age);
        return introduce;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object otherPerson) {
        Person person = (Person) otherPerson;
        return this.id == person.id;
    }

    public void speak(Student student){

    }

    public String getName() {
        return name;
    }
}
